package com.ml.chatbot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableDiscoveryClient
@EnableScheduling
public class ChatbotServiceApplication {

  public static void main(final String[] args) {

    final SpringApplication springApplication = new SpringApplication(ChatbotServiceApplication.class);
    springApplication.run(args);
  }

}

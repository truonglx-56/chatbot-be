#!/usr/bin/env bash

env=$1
case "$env" in
  int)
    branch=develop
    ;;
  stg)
    branch=master
    ;;
  prd)
    branch=master
    ;;
  local)
    branch="$2"
    ;;
  *)
    echo "First argument must be local/int/stg/prd. Exit deployment!"
    exit 1
esac

modules=(config eureka chatbot-service)
user=gitlab-runner

for opt in "$@"; do
  case "$opt" in
    --ignoreBranch)
      ignoreBranch=true
      ;;
    --ignoreBuild)
      ignoreBuild=true
      ;;
    --modules*)
      modulesOpts=${opt#*=}
      IFS="," read -r -a modules <<< "$modulesOpts"
      ;;
    --user*)
      user=${opt#*=}
      ;;
  esac
done

checkSkipCd=$(git log -n 1 | grep "skip cd" | wc -l)
if [[ "$checkSkipCd" -gt 0 ]]; then
  echo "Ignore deploying because of skip cd!"
  exit
fi

echo "Deploy modules: ${modules[@]} by user ${user}"

currentBranch=$(git branch | grep \* | cut -d ' ' -f2)
if [[ "$env" != "local" && "$ignoreBranch" != true && "$branch" != "$currentBranch" ]]; then
  echo "Environment ${env} must be deployed from branch ${branch}."
  echo "If you want to deploy other branch, please use option --ignoreBranch."
  echo "Stop deployment!"
  exit
fi

if [[ "$ignoreBuild" != true ]]; then
  mvn clean install
fi

for module in "${modules[@]}"; do
  host=$(cat script/servers.text | grep "$module $env" | cut -d ' ' -f 3)
  jarFile=$(cat script/module-mapping.text | grep "$module" | cut -d = -f 2)
  des=/opt/pipeline/${module}/
  if [[ "$env" == "local" ]]; then
    cp ${jarFile} ${des}
    echo "Restart service $module.."
    systemctl restart pipeline-${module}
  else
    jarFileName=${jarFile##*/}
    remoteDeployFileName=remote-deploy-${module}.sh
    cat > script/${remoteDeployFileName} << EOF
#!/usr/bin/env bash
cp -f ${jarFileName} ${des}
sudo systemctl restart pipeline-${module}
EOF
    sshpass -e scp -o stricthostkeychecking=no ${jarFile} ${user}@${host}:~/
    echo "Copied $jarFile to $host."
    sshpass -e scp -o stricthostkeychecking=no script/${remoteDeployFileName} ${user}@${host}:~/
    echo "Execute restart service $module.."
    sshpass -e ssh ${user}@${host} "bash ${remoteDeployFileName}"
  fi
  if [[ "$module" == "config" || "$module" == "eureka" ]]; then
    sleep 30s
  fi
done

echo "Finished deployment!"
echo


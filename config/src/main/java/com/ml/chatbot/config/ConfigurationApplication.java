package com.ml.chatbot.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigurationApplication {

  public static void main(final String[] args) {

    SpringApplication.run(ConfigurationApplication.class, args);
  }

}
